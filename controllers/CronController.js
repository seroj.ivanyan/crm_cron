const db = require('../db');

const {
  States: StatesModel,
  Addresses: AddressesModel,
  Cities: CitiesModel,
} = db;

class CronController {
  constructor() {
    this.insertStates = this.insertStates.bind(this);
    this.getStatesIds = this.getStatesIds.bind(this);
    this.insertAddresses = this.insertAddresses.bind(this);
    this.deleteAddresses = this.deleteAddresses.bind(this);
  }

  async insertStates(data) {
    const states = await StatesModel.bulkCreate(data);
    return states;
  }

  async insertCities(data) {
    const states = await CitiesModel.bulkCreate(data);
    return states;
  }

  async insertAddresses(data) {
    console.log('addresses deleted');
    await this.deleteAddresses();
    console.log('addresses bulkCreate');

    const addresses = await AddressesModel.bulkCreate(data);
    console.log(addresses)
    console.log('addresses bulkCreate finished');

    return addresses;
  }

  async deleteAddresses() {
    const addresses = await AddressesModel.destroy({ where: { } });
    return addresses;
  }

  async getStatesIds() {
    const states = await StatesModel.findAll({});
    return states.map((item) => item.hayPostId);
  }
}

module.exports = new CronController();
