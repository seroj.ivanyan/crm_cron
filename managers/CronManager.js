const { CronJob } = require('cron');
const axios = require('axios');
const moment = require('moment');
const cronController = require('../controllers/CronController');

const lastCronSuccessTime = null;
const lastCronFailureTime = null;
const checkStatus = {
  isStateInserted: false,
  isAddressInserted: false,
};
class CronManager {
  constructor() {
    this.initialize = this.initialize.bind(this);
    this.insertStates = this.insertStates.bind(this);
    this.getAddresses = this.getAddresses.bind(this);
    this.makePromiseAllArray = this.makePromiseAllArray.bind(this);
  }

  async insertStates() {
    try {
      if (!checkStatus.isStateInserted) {
        const receivedData = await axios.get('https://api.haypost.am/page/92?lng=am', { timeout: 30000 });

        if (receivedData && receivedData.data.module && receivedData.data.module.region) {
          const states = receivedData.data.module.region.map(
            (item) => ({
              name: item.name,
              hayPostId: item.id,
              cities: item.cities,
            }),
          );
          const citiesData = [];
          receivedData.data.module.region.forEach((item) => {
            item.cities.forEach((citiItem) => {
              citiesData.push(
                {
                  name: citiItem.name,
                  hayPostId: citiItem.id,
                  hayPostStateId: item.id,

                },
              );
            });
          });

          await cronController.insertStates(states);
          await cronController.insertCities(citiesData);
          checkStatus.isStateInserted = true;
        }
      } else {
        console.log('State already inserted');
      }
    } catch (e) {

    }
  }

  makePromiseAllArray(ids) {
    const promisedArray = [];

    ids.forEach((id) => promisedArray.push(axios.get(`https://api.haypost.am/postalIndex?region=${id}&lng=am`, { timeout: 30000 })));
    return promisedArray;
  }

  async getAddresses(id) {
    const receivedData = await axios.get(`https://api.haypost.am/postalIndex?region=${id}&lng=am`, { timeout: 30000 });
    return receivedData.data;
  }

  initialize() {
    // eslint-disable-next-line no-new
    new CronJob('*/30 * * * * * ', (async () => {
      // if need update states remove comments
      this.insertStates();
      const stateIds = await cronController.getStatesIds();
      const response = await Promise.all(this.makePromiseAllArray(stateIds));
      const fullAddressData = [];
      response.forEach((item) => {
        fullAddressData.push(...item.data);
      });

      const addressData = fullAddressData.map((add) => ({
        hayPostId: add.id,
        hayPostCityId: add.city,
        hayPostStateId: add.region,
        postal_code: add.postal_code,
        name: add.address,
      }));
      await cronController.insertAddresses(addressData);
    }), null, true, 'Asia/Yerevan');
  }
}

module.exports = new CronManager();
