/* eslint-disable no-param-reassign */
const DataTypes = require('sequelize');

/**
 *
 * @type {Sequelize}
 */

const AddressesSchema = {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: DataTypes.INTEGER,
  },
  name: {
    type: DataTypes.STRING,
  },
  postal_code: {
    type: DataTypes.STRING,
  },
  hayPostId: {
    type: DataTypes.INTEGER,
  },
  hayPostStateId: {
    type: DataTypes.INTEGER,
  },
  hayPostCityId: {
    type: DataTypes.INTEGER,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: DataTypes.NOW,
  },
};

const AddressesOptions = {
  timestamps: false,
  freezeTableName: true,

};

module.exports = (seq) => {
  const model = seq.define('Addresses', AddressesSchema, AddressesOptions);
  // model.associate = BoardDomainMapAssociation;
  return model;
};
