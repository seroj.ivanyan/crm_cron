/* eslint-disable no-param-reassign */
const DataTypes = require('sequelize');

/**
 *
 * @type {Sequelize}
 */

const CitiesSchema = {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: DataTypes.INTEGER,
  },
  name: {
    type: DataTypes.STRING,
  },
  hayPostId: {
    type: DataTypes.INTEGER,
  },
  hayPostStateId: {
    type: DataTypes.INTEGER,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: DataTypes.NOW,
  },
};

const CitiesOptions = {
  timestamps: false,
  freezeTableName: true,

};

module.exports = (seq) => {
  const model = seq.define('Cities', CitiesSchema, CitiesOptions);
  // model.associate = BoardDomainMapAssociation;
  return model;
};
