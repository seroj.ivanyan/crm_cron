/* eslint-disable no-param-reassign */
const DataTypes = require('sequelize');

/**
 *
 * @type {Sequelize}
 */

const StatesSchema = {
  id: {
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
    type: DataTypes.INTEGER,
  },
  name: {
    type: DataTypes.STRING,
  },
  cities: {
    type: DataTypes.JSONB,
  },
  hayPostId: {
    type: DataTypes.INTEGER,
  },
  createdAt: {
    type: DataTypes.DATE,
    defaultValue: DataTypes.NOW,
  },
};

const StatesOptions = {
  timestamps: false,
  freezeTableName: true,

};

module.exports = (seq) => {
  const model = seq.define('States', StatesSchema, StatesOptions);
  return model;
};
